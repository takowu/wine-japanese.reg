# wine-japanese.reg

Wineで日本語の文字をキレイに表示させるためのレジストリ。レジストリエディタからインポートすればおｋ。原作は https://gist.github.com/nogajun/6095ed8488ef9449e63dc1ce578ae55e

fonts.confはビットマップフォントを無効にする設定。~/.config/fontconfig/に置く。